module Pichou.Activity

open System
open Pichou.Config
open DSharpPlus
open DSharpPlus.Entities


let defaultActivity =
    DiscordActivity("P! help", ActivityType.ListeningTo)

let scheduleActivity (client: DiscordClient) =
    let intervale = appConfig.["activityIntervale"] |> float
    let botVersion = appConfig.["botVersion"]

    let timer = new Timers.Timer(intervale)
    let event = Async.AwaitEvent (timer.Elapsed) |> Async.Ignore

    timer.Start()
    while true do
        Async.RunSynchronously event

        let currentActivity = client.CurrentUser.Presence.Activity
        let newActivity =
            match currentActivity.ActivityType with
            | ActivityType.ListeningTo ->
                let name = sprintf "Version: %s" botVersion
                DiscordActivity(name, ActivityType.Watching)
            | ActivityType.Watching
            | _ -> defaultActivity

        client.UpdateStatusAsync newActivity
        |> Async.AwaitTask
        |> Async.RunSynchronously