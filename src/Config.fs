module Pichou.Config

open System.IO
open dotenv.net
open Microsoft.Extensions.Configuration

let appConfig =
    ConfigurationBuilder()
        .SetBasePath(Directory.GetCurrentDirectory())
        .AddJsonFile("AppSettings.json", false, true)
        .Build()

let envVar =
    DotEnv.Load()

    ConfigurationBuilder()
        .AddEnvironmentVariables()
        .Build()