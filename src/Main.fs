module Pichou.Main

open Pichou.Config
open DSharpPlus
open DSharpPlus.CommandsNext
open DSharpPlus.SlashCommands

[<EntryPoint>]
let main _argv =
    printfn "Starting"

    let token = envVar.["BOT_TOKEN"]
    let botConfig = DiscordConfiguration()

    botConfig.Token <- token
    botConfig.TokenType <- TokenType.Bot

    let client = new DiscordClient(botConfig)


    let commandConfig = CommandsNextConfiguration()
    commandConfig.StringPrefixes <- [ "P!"; "P!chou" ]
    commandConfig.CaseSensitive <- false
    commandConfig.IgnoreExtraArguments <- true
    commandConfig.EnableMentionPrefix <- true

    let commands = client.UseCommandsNext(commandConfig)
    commands.RegisterCommands<Pichou.Commands>()

    let slashCommands = client.UseSlashCommands()
    slashCommands.RegisterCommands<Pichou.SlashCommands>()


    client.ConnectAsync(Activity.defaultActivity)
    |> Async.AwaitTask
    |> Async.RunSynchronously

    Activity.scheduleActivity client

    1